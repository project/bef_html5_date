# BEF HTML5 Date plugin

BEF plugin allowing to use HTML5 date fields in views exposed filters.

For a full description of the module, please visit the
[project page](https://www.drupal.org/project/bef_html5_date).

To submit bug reports and feature suggestions or to track changes, visit the
[issue queue](https://www.drupal.org/project/issues/bef_html5_date).


## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)


## Requirements

This module requires the following modules:

- [Better Exposed Filters](https://www.drupal.org/project/better_exposed_filters)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module does not have any menu or modifiable settings. There is no
configuration needed.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
